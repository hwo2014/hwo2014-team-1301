module Logic where

import Data.Maybe (fromJust)
import Control.Monad
import Text.Printf

import Config
import GameInitModel
import CarPositionsModel
import GameEndModel

data GameData = GameData GameInitData MyCarData

type ClientMessage = String                                   -- vvv gameTick
data ServerMessage = Join | GameInit GameInitData | CarPositions Int [CarPosition]
                        | LapFinished LapFinishedData | GameEnd GameEndData | Unknown String

-- speed = length units per tick
data MyCarData = MyCarData {prevData :: PreviousData, speed :: Float}
data PreviousData = PreviousData {inPieceDistanceBackThen :: Float, speedBackThen :: Float}

throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ show amount ++ "}"
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"

myCarPosition :: [CarPosition] -> PiecePosition
myCarPosition = piecePosition . fromJust . findCar carName

limitAt01 r
    | r < 0 = 0
    | r > 1 = 1
    | otherwise = r

poso r = if r<0 then 0 else r

bars = "-------------------------------------------------------- "

respond :: GameData -> ServerMessage -> IO (GameData, [ClientMessage])
respond gd@(GameData gameInitData oldCarData) message = case message of
  Join -> do
    putStrLn $ bars ++ "Joined"
    return (gd, [pingMessage])
  GameInit gameInit -> do
    putStrLn $ bars ++ "GameInit: " ++ reportGameInit gameInit
    return (gd, [pingMessage])
  CarPositions gameTick carPositions -> do
    -- Report progress
    --when (mod gameTick 100 == 0) $ putStrLn $ show gameTick ++ " / " ++ (show $ players gameInitData)
    let newIpd = inPieceDistance . myCarPosition $ carPositions
    let newMinusOldIpd = newIpd - (inPieceDistanceBackThen . prevData $ oldCarData)
    -- Update car data
    let updatedCarData = MyCarData (PreviousData newIpd $ speed oldCarData)
            (if newMinusOldIpd >= 0 then newMinusOldIpd else speedBackThen . prevData $ oldCarData)
    let newgamedata = GameData gameInitData updatedCarData
    -- Report piece jumps
    when (newMinusOldIpd < 0) $ putStrLn $
        printf "%2i" (pieceIndex . myCarPosition $ carPositions)
        ++ " / Speed: " ++ printf "%6.3f" (speed updatedCarData)
        ++ " / Throttle: " ++ printf "%6.3f" (throttleFormula newgamedata carPositions)
        ++ " / AngleDiff: " ++ printf "%5.1f" (angle . fromJust . findCar carName $ carPositions)
    return (newgamedata, carPositionsResponses newgamedata gameTick carPositions)
  LapFinished lapData -> do
    putStrLn $ bars ++ "Lap finished / Millis: "
        ++ (show . lapmillis . lapfinlapTime $ lapData)
    return (gd, [pingMessage])
  GameEnd gameEnd -> do
    putStrLn $ bars ++ "GameEnd / Best Lap Millis: "
        ++ (show . lapmillis . lapresult . head . bestLaps $ gameEnd)
    return (gd, [pingMessage])
  Unknown msgType -> do
    putStrLn $ bars ++ "Unknown message: " ++ msgType
    return (gd, [pingMessage])

carPositionsResponses :: GameData -> Int -> [CarPosition] -> [ClientMessage]
carPositionsResponses gameData gameTick carPositions
    | gameTick < 0 = []
    | otherwise    = [throttleMessage . limitAt01 $ throttleFormula gameData carPositions]
                        --, "{\"msgType\": \"switchLane\", \"data\": \"Right\"}"]

throttleFormula :: GameData -> [CarPosition] -> Float
throttleFormula (GameData gameInitData myCarData) carPositions = (sum $ zipWith (*) weights wishes) / (sum weights)
    where
        -- old config had 30, 125, 120
        -- other config had 20, 50, 48, 50
        steps = 30
        a = 125
        b = 120
        distance = min 3 $ 12 / (friction*friction)
        -- e^((20/50)(x-10))/(1+e^((20/48)(x-10))) from 0 to 19 -- aka logistic growth
        weights = map (\x -> exp (steps/a*(x-steps/2)) / (1 + exp (steps/b*(x-steps/2)))) [steps-1,steps-2..0]
        wishes = map (rateDanger . findPieceComingIn) [distance, 2*distance..]
        
        myCarPos = myCarPosition $ carPositions
        findPieceComingIn dist = if dist < remainingInCurrent
                                    then currentPiece
                                    else snd $ foldl pieceWalker (dist - remainingInCurrent, undefined) $ tail comingPieces
        pieceWalker (dist, oldpiece) piece
            | dist < 0 = (dist, oldpiece)
            | otherwise = (dist - pieceWay piece, piece)
        currentTrack = track . race $ gameInitData
        trackList = pieces currentTrack
        comingPieces = drop (pieceIndex myCarPos) trackList ++ trackList
        currentPiece = head comingPieces
        remainingInCurrent = pieceWay currentPiece - inPieceDistance myCarPos

        speedDiff = speed myCarData - 6.66
        carAngle = angle . fromJust . findCar carName $ carPositions
        includeFriction x = (x-5) / friction + 5
        friction = case chosenTrackName of
                       "keimola" -> 1.65
                       "germany" -> 0.9
                       "usa"     -> 999999999
                       "france"  -> 0.95
                       _         -> 0.7

        rateDanger piece = case pieceRadius piece of
                            Nothing -> 1.0 - speedDiff / 30 - carAngle*carAngle/5000
                            Just r  -> includeFriction $ negate speedDiff * 0.4
                                         - exp (negate . abs $ laneRadius r / 5000.0) * exp speedDiff *
                                         (1 + 1 / (12 - (abs carAngle / 5)) *
                                            (if signum carAngle /= signum (-thisPieceAngle) then 20 else 7))
            where
                thisPieceAngle =  fromJust (pieceAngle piece)
                laneRadius r = (if thisPieceAngle < 0 then (+) else (-)) r $ fromIntegral . distanceFromCenter
                                . head . filter ((==) (endLaneIndex . lane $ myCarPos) . index) . lanes $ currentTrack
