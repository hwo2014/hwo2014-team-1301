{-# LANGUAGE OverloadedStrings #-}

module GameEndModel where

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))

import GameInitModel (CarId)

-- OverallResultData

data OverallResultData = OverallResultData {
	overalllaps :: Int,
	overallticks :: Int,
	overallmillis :: Int
} deriving (Show)

instance FromJSON OverallResultData where
  parseJSON (Object v) =
    OverallResultData <$>
    (v .: "laps") <*>
    (v .: "ticks") <*>
    (v .: "millis")

-- LapResultData

data LapResultData = LapResultData {
	laplap :: Int,
	lapticks :: Int,
	lapmillis :: Int
} deriving (Show)

instance FromJSON LapResultData where
  parseJSON (Object v) =
    LapResultData <$>
    (v .: "lap") <*>
    (v .: "ticks") <*>
    (v .: "millis")

-- OverallResult

data OverallResult = OverallResult {
	overallcar :: CarId,
	overallresult :: OverallResultData
} deriving (Show)

instance FromJSON OverallResult where
  parseJSON (Object v) =
    OverallResult <$>
    (v .: "car") <*>
    (v .: "result")

-- LapResult

data LapResult = LapResult {
	lapcar :: CarId,
	lapresult :: LapResultData
} deriving (Show)

instance FromJSON LapResult where
  parseJSON (Object v) =
    LapResult <$>
    (v .: "car") <*>
    (v .: "result")

-- GameEndData

data GameEndData = GameEndData {
  results :: [OverallResult],
  bestLaps :: [LapResult]
} deriving (Show)

instance FromJSON GameEndData where
  parseJSON (Object v) =
    GameEndData <$>
    (v .: "results") <*>
    (v .: "bestLaps")

------------------------------------------

-- LapFinishedData

data LapFinishedData = LapFinishedData {
  lapfincar :: CarId,
  lapfinlapTime :: LapResultData,
  lapfinraceTime :: OverallResultData
  -- TODO: ranking definitions
} deriving (Show)

instance FromJSON LapFinishedData where
  parseJSON (Object v) =
    LapFinishedData <$>
    (v .: "car") <*>
    (v .: "lapTime") <*>
    (v .: "raceTime")