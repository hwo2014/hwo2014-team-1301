{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), (.:?), Result(..))

import Config
import Logic
import GameInitModel
import CarPositionsModel
import GameEndModel

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> run server port botname botkey
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run server port botname botkey = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  --hPutStrLn h $ "{\"msgType\": \"joinRace\", \"data\": { \"botId\": { \"name\":\""
  --  ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\" }, \"trackName\": \"" ++ chosenTrackName ++ "\", \"carCount\": 1 }}"
  hPutStrLn h $ "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
  handleMessages startData h
    where startData = GameData EmptyGameData $ MyCarData (PreviousData 9000.0001 0.0) 0.0


handleMessages :: GameData -> Handle -> IO ()
handleMessages (GameData oldgameinitdata oldcardata) h = do
  msg <- hGetLine h
  --putStrLn msg
  case decode (L.pack msg) of
    Just json ->
      let decodedServerMessage = fromJSON json >>= decodeMessage in
      case decodedServerMessage of
        Success serverMessage -> handleServerMessage h serverMessage $ GameData (possiblyNewGameInitData serverMessage) oldcardata
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> fail $ "Error parsing JSON: " ++ show msg
  where
    possiblyNewGameInitData serverMessage = case serverMessage of
                                              GameInit freshgameinitdata -> freshgameinitdata
                                              _                          -> oldgameinitdata

handleServerMessage :: Handle -> ServerMessage -> GameData -> IO ()
handleServerMessage h serverMessage gamedata = do
  (newgamedata, responses) <- respond gamedata serverMessage
  forM_ responses $ hPutStrLn h
  handleMessages newgamedata h

decodeMessage :: (String, Value, Value) -> Result ServerMessage
decodeMessage (msgType, msgData, gameTick)
  | msgType == "joinRace" = Success Join
  | msgType == "gameInit" = GameInit <$> fromJSON msgData
  | msgType == "carPositions" = CarPositions <$> fromJSON gameTick <*> fromJSON msgData
  | msgType == "lapFinished" = LapFinished <$> fromJSON msgData
  | msgType == "gameEnd" = GameEnd <$> fromJSON msgData
  | otherwise = Success $ Unknown msgType

instance (FromJSON a) => FromJSON (String, a, Value) where
  parseJSON (Object v) = do
    msgType  <- v .: "msgType"
    msgData  <- v .: "data"
    maybeGameTick <- v .:? "gameTick"
    case maybeGameTick of
      Just gameTick -> return (msgType, msgData, gameTick)
      Nothing       -> return (msgType, msgData, Number (-1)) -- Not all messages contain gameTick
  parseJSON x          = fail $ "Not an JSON object: " ++ show x
